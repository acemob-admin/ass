﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTableScheduler
{

    public class EmployeePool
    {

        private static int _chooseCount = 0;
        Random _rnd = new Random();
        public List<Employee> _empCollection = new List<Employee>();

        public void Add(Employee emp)
        {
            _empCollection.Add(emp);
        }

        public Employee[] Get() { return _empCollection.ToArray(); }

        public Employee ChooseEmployee(DateTime startDate, uint day, string col, TimeTable tt)
        {

            var newList = _empCollection.OrderBy(o => o.TotalWorkingDay).ToList(); //sort the employee with less working day first


            //int num = _rnd.Next(0, newList.Count);//randomize

            //List<Employee> foundByOrder = new List<Employee>();
            Dictionary<Employee, int> foundByOrder = new Dictionary<Employee, int>();
            foreach (Employee emp in newList)
            {
                foundByOrder[emp] = 0;

                //check if the employee schedule is fullfiled
                if (emp.Leaves.Contains(day))
                    continue;// clashes with the leave

                foundByOrder[emp]++;

                if (col.Equals("col2")
                    && tt.GetEmployee(day, "col1") != null
                    && tt.GetEmployee(day, "col1") == emp)
                    continue; // same employee should not continuously working in col1,col2 for same day

                foundByOrder[emp]++;

                DateTime currentDate = startDate.Add(new TimeSpan((int)(day - 1), 0, 0, 0));
                bool worksInSameDayOfWeek = false;
                foreach (uint workDay in emp.WorkingDays)
                {
                    DateTime workDate = startDate.Add(new TimeSpan((int)(workDay - 1), 0, 0, 0));
                    if (workDate.DayOfWeek == currentDate.DayOfWeek)
                    {
                        worksInSameDayOfWeek = true;
                        break;
                    }
                }

                if (worksInSameDayOfWeek == true)
                    continue;

                foundByOrder[emp]++;

                //return the employee

                //return emp;
            }

            if (foundByOrder.Count > 0)
            {
                var valuesList = foundByOrder.Values.ToList();
                valuesList.Sort(
                     delegate(int a, int b)
                     {
                         if (a >= b) return -1;
                         return 0;

                     }
                 );
                int firstValue = valuesList.First();
                var foundPair = foundByOrder.First(
                    delegate(KeyValuePair<Employee, int> o)
                    {
                        if (o.Value == firstValue)
                            return true;
                        return false;
                    });
                return foundPair.Key;
            }

            return null;//can't find suitable employee...

        }
    }


}
