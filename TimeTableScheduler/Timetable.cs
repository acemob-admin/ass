﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TimeTableScheduler
{
    /// <summary>
    /// data model of time table
    /// </summary>
    public class TimeTable
    {
        private Dictionary<uint, Dictionary<string, Employee>> _data
            = new Dictionary<uint, Dictionary<string, Employee>>();


        uint _days; String[] _columes;

        public TimeTable(uint days, String[] columes)
        {
            if (days > 0 && columes.Length > 0)
            {
                _days = days;
                _columes = columes;
                InitTable();
            }
        }

        public uint Days { get { return _days; } }

        private void InitTable()
        {
            for (uint count = 1; count <= _days; count++)
            {
                AddRow(count);
            }
        }

        private void AddRow(uint day)
        {

            if (day <= _days)
            {
                _data.Add(day, new Dictionary<String, Employee>());
                foreach (String col in _columes)
                {

                    _data[day].Add(col, null);
                }

            }
        }


        /// <summary>
        /// get empty slot 
        /// </summary>
        /// <param name="colName"></param>
        /// <returns></returns>
        public uint[] GetEmptySlotByColume(string colName)
        {
            List<uint> dayList = new List<uint>();
            foreach (uint day in _data.Keys)
            {
                if (_data[day].ContainsKey(colName)
                    && _data[day][colName] == null)
                    dayList.Add(day);
            }
            return dayList.ToArray();
        }

        public Employee GetEmployee(uint day, string colName)
        {
            if(_data.ContainsKey(day)
                && _data[day].ContainsKey(colName)
                && _data[day][colName] != null)
                return _data[day][colName];

            return null;
        }

        public bool SetEmployee(uint day, string colName, Employee emp)
        {
            if (_data.ContainsKey(day)
                && _data[day].ContainsKey(colName))
            {
                _data[day][colName] = emp;
                return true;
            }

            return false;
        }



        /// <summary>
        /// retrieve the emplployee in slot
        /// </summary>
        /// <param name="day"></param>
        /// <param name="colName"></param>
        /// <returns></returns>
        public Employee GetEmployeeInSlotByColume(uint day, string colName)
        {
            if (_data.ContainsKey(day) && _data[day].ContainsKey(colName))
            {
                return _data[day][colName];
            }
            return null;
        }


    }


}
